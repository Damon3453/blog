<?php

namespace App\Models\Api;

use App\Traits\PaginationTrait;

use App\Models\Post as BasePost;

/**
 *
 */
class Post extends BasePost
{
    use PaginationTrait;

    public function scopeWithFilters($query, $filters)
    {
        $q = $query->where('id', 'like', "%{$filters->fields->id}%");

        if ($filters->fields->heading) {
            $q->where('create_from', $filters->fields->heading);
        }

        if ($filters->fields->status) {
            $q->where('status', $filters->fields->status);
        }

        if ($filters->fields->date_range) {
            $q->whereBetween('published_at', [$filters->fields->date_range[0], $filters->fields->date_range[1]]);
        }

        return $q;
    }

    public static function getStatuses()
    {
        return [
            ['id' => 0, 'name' => 'Не опубликован'],
            ['id' => 1, 'name' => 'Опубликован']
        ];
    }

    public function getStatusAsString()
    {
        switch ($this->active) {
            case true:
                return 'Опубликован';

            default:
                return  'Не опубликован';
        }
    }

    public function getActiveClassLabel()
    {
        switch ($this->active) {
            case true:
                return "badge-success";

            default:
                return "badge-default";
        }
    }

    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }

    public function makeTranslit($string) {
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );

    return strtr($string, $converter);
    }

    public function makeAlias($str) {
        $str = strtolower($this->makeTranslit($str));
        $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
        $str = trim($str, "-");

        return $str;
    }
}
