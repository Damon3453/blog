<?php

namespace App\Models\Api;

use App\Traits\PaginationTrait;

use App\User as BaseUser;

class User extends BaseUser
{
    use PaginationTrait;

    public function scopeWithFilters($query, $filters)
    {
        $q = $query->where('id', 'like', "%{$filters->fields->id}%");

        if ($filters->fields->name) {
            $q->where('name', 'like', "%{$filters->fields->name}%");
        }
        if ($filters->fields->surname) {
            $q->where('surname', 'like', "%{$filters->fields->surname}%");
        }
        if ($filters->fields->country) {
            $q->where('country', 'like', "%{$filters->fields->country}%");
        }
        if ($filters->fields->phone) {
            $q->where('phone', 'like', "%{$filters->fields->phone}%");
        }

        return $q;
    }
}
