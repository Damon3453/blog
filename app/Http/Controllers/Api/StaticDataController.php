<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Api\Post;

class StaticDataController extends Controller
{
    public function getStatuses()
    {
        return Post::getStatuses();
    }
}
