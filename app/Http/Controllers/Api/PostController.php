<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Http\Requests\PostRequest;
use App\Http\Resources\CountableCollection;
use App\Http\Resources\Posts\IndexResource;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Throwable;

// Models
use App\Models\Api\Post;

class PostController extends Controller
{
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return CountableCollection
     */
    public function index(Request $request)
    {
        $filters = json_decode($request->filters);
        $posts = Post::withFilters($filters);//->withTrashed();
        $total = (clone $posts)->count();
        $posts = $posts->paginationApi($filters)->get();

        return new CountableCollection($posts, $total, IndexResource::class);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PostRequest $request
     * @throws Throwable
     *
     * @return JsonResponse
     */
    public function store(PostRequest $request)
    {
        try {
            $data = [
                'alias' => $this->post->makeAlias($request->heading),
                'published_at' => (new \DateTime())->format('Y-m-d H:i:s')
            ];
            Post::create($request->validated() + $data);

            return response()->json(['message' => 'Пост добавлен в блог'], 200);
        } catch (Throwable $e) {
            DB::rollback();
            Log::debug($e->getMessage());

            return response()->json(['message' => 'Ошибка создания поста'], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return array
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        return [
            'id'                => $post->id,
            'heading'           => $post->heading,
            'alias'             => $post->alias,
            'content'           => $post->content,
            'active'            => $post->active
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostRequest $request
     * @param int $id
     * @throws Throwable
     *
     * @return JsonResponse
     */
    public function update(PostRequest $request, $id)
    {
        $post = Post::findOrFail($id);
        try {
            $data = [
                'published_at' => (new \DateTime())->format('Y-m-d H:i:s')
            ];
            if ($post->heading != $request->heading) {
                $data += ['alias' => $this->post->makeAlias($request->heading)];
            }
            $post->update($request->validated() + $data);

            return response()->json(['message' => 'Ура! Пост обновлён!'], 200);
        } catch (Throwable $e) {
            DB::rollback();
            Log::debug($e->getMessage());

            return response()->json(['message' => 'Ошибка обновления поста'], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
