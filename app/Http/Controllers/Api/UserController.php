<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Resources\CountableCollection;
use App\Http\Resources\Users\IndexResource;

use App\Models\Api\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return CountableCollection
     */
    public function index(Request $request)
    {
        $filters = json_decode($request->filters);
        $users = User::withFilters($filters);
        $total = (clone $users)->count();
        $users = $users->paginationApi($filters)->get();

        return new CountableCollection($users, $total, IndexResource::class);
    }
}
