<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Models\Api\Post;
use Illuminate\Validation\Rule;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'heading' => 'required|string|max:255',
            'content' => 'required|string|max:65535',
            'active' => 'required|boolean',
        ];
        if ($this->getMethod() == 'POST') {
            $this->alias = (new Post)->makeAlias($this->heading);
            $rules += ['alias' => 'unique:posts,alias'];
        } else {
            $rules += ['alias' => Rule::unique('posts')->ignore($this->id)];
        }

        return $rules;
    }
}
