<?php

namespace App\Http\Resources\Posts;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @throws \Exception
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'heading'           => $this->heading,
            'content'           => $this->content,
            'active'            => $this->getStatusAsString(),
            'active_label'      => $this->getActiveClassLabel(),
            'image'             => $this->image,
            'alt'               => $this->alt,
            'published_at'      => (new \DateTime($this->published_at))->format('d-m-Y')
        ];
    }
}
