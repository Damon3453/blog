<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 *
 */
class CountableCollection extends ResourceCollection
{
    protected $totalItems = 0;
    protected static $resourceClass = JsonResource::class;

    public function __construct($collection, int $total = 0, string $resourceClass = JsonResource::class)
    {
        parent::__construct($collection);

        $this->totalItems = $total;
        static::$resourceClass = $resourceClass;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection = [];

        foreach ($this->collection as $key => $item) {
            $collection[] = new static::$resourceClass($item);
        }

        return [
            'total' => $this->totalItems,
            'data' => $collection,
        ];
    }
}
