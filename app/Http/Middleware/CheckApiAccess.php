<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckApiAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->hasHeader('X-API-KEY')) {
            return response()->json(['message' => 'Пользователь не авторизован'], 403);
        }

        $token = $request->header('X-API-KEY');

        $user = User::where('auth_token', $token)
            ->where('auth_token_expire', '>', (new \DateTime)->format('Y-m-d H:i:s'))
            ->first();

        if (!$user) {
            return response()->json(['message' => 'Пользователь не найден или истек срок действия сессии.'], 403);
        }

        Auth::login($user, true);

        return $next($request);
    }
}
