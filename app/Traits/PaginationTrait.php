<?php

namespace App\Traits;

trait PaginationTrait
{
    public function scopePaginationApi($query, $filters)
    {
        $q = $query->limit($filters->pagination->limit ?? 20)
            ->offset($this->getOffset($filters->pagination->page, $filters->pagination->limit ?? 20));

        $q->orderBy($filters->sort->field, $filters->sort->order ? 'ASC' : 'DESC');
    }


    public function getOffset(int $page, int $limit)
    {
        return $page == 1 ? 0 : ($page - 1) * $limit;
    }
}
