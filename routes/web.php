<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::prefix('admin')->group(function () {
    Route::get('/login', 'Auth\LoginController@showLoginForm');
    Route::post('/login', 'Auth\LoginController@login')->name('login');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

//    Route::middleware('can:accessAdminpanel')->group(function() {
        Route::get('/', 'Admin\MainController@index');
//    });

    Route::view('/posts', 'admin.posts.index');
    Route::view('/users', 'admin.users.index');
});

Route::get('/home', 'HomeController@index')->name('home');
