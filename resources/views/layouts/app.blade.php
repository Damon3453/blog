 <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <base href="{{ env('APP_URL') }}]" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ env('APP_NAME') }}</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    @yield('styles')
</head>
<body>
    <div class="site-wrap">
    @include('layouts._partials.header')

    @include('sections.main-slider')
    @include('sections.welcome-words')
    @include('sections.blog')

    @include('layouts._partials.footer')

    @yield('scripts')
    </div>
</body>
</html>
