<div class="slide-one-item home-slider owl-carousel">

    <div class="site-blocks-cover overlay" style="background-image: url({{ asset('images/hero_1.jpg') }});" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-7 text-center" data-aos="fade">
                    <h2 class="caption mb-2">Inspiration for everybody</h2>
                    <h1 class="">Welcome To MarinaMonterey's blog</h1>

                </div>
            </div>
        </div>
    </div>

    <div class="site-blocks-cover overlay" style="background-image: url({{ asset('images/hero_2.jpg') }});" data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-7 text-center" data-aos="fade">
                    <h2 class="caption mb-2">Enjoy With Me</h2>
                    <h1 class="">Love &amp; Nature</h1>
                </div>
            </div>
        </div>
    </div>
</div>
