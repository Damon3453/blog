<div id="#blog" class="site-section bg-light">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mx-auto text-center mb-5 section-heading">
                <h2 class="mb-5">Блог</h2>
            </div>
        </div>
        <div class="row">
            @foreach($posts as $post)
            <div class="col-md-6 col-lg-12 mx-auto text-center mb-5">
                <div class="program">
                    <a href="#" class="d-block mb-0 thumbnail">
                        <img src="{{ asset('images/img_1.jpg') }}" alt="Image" class="img-fluid">
                    </a>
                    <div class="program-body">
                        <h3 class="heading mb-2">{{ $post->heading }}</h3>
                        <p class="blog-p">{{ $post->content }}</p>
                        <div class="span">
                            <span class="mr-4">Posted by <a href="https://www.instagram.com/salem_cookie/" class="href" target="_blank">MM</a></span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
