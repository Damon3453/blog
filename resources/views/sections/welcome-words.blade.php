<div class="site-block-half d-flex">
    <div class="image bg-image" style="background-image: url({{ asset('images/img_1.jpg') }});"></div>
    <div class="text">
        <h2 class="font-family-serif">Welcome To Yogalife</h2>
        <span class="caption d-block text-primary pl-0 mb-4">Hello there!</span>
        <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti odio laboriosam in recusandae expedita ducimus voluptatum provident doloremque doloribus, deserunt, ad maxime voluptas voluptatem ex qui quam saepe debitis dolorum!</p>
        <p><a href="#" class="btn btn-primary pill px-4 py-3 text-white">See The Yoga Pricing</a></p>

    </div>
</div>
