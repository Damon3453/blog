@extends('adminlte::page')

@section('title', 'Блог')

@section('content_header')
@stop

@section('plugins.Sweetalert2', true)

@section('content')
    <div id="app">
        <router-view></router-view>
    </div>
@stop

@section('css')
{{--    <link rel="stylesheet" href="/css/admin_custom.css">--}}
@stop

@section('js')
    <script src="{{ mix('js/admin/users/app.js') }}" charset="utf-8"></script>
@stop
