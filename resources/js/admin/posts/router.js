import VueRouter from 'vue-router';

import NotFound from './common/NotFound.vue'
import OrderIndex from './pages/Index.vue'
import OrderForm from './pages/Form.vue'

const routes = [
    {
        path: '*',
        components: {
            default: NotFound
        }
    },
    {
        path: '/',
        name: 'index',
        components: {
            // header: Header,
            default: OrderIndex,
            // footer: Footer
        }
    },
    {
        path: '/create',
        name: 'create',
        components: {
            // header: Header,
            default: OrderForm,
            // footer: Footer
        }
    },
    {
        path: '/update/:id',
        name: 'update',
        components: {
            // header: Header,
            default: OrderForm,
            // footer: Footer
        }
    },
    /*{
        path: '/view/:id',
        name: 'view',
        components: {
            // header: Header,
            default: OrderView,
            // footer: Footer
        }
    },*/
    {
        path: '/delete/:id',
        name: 'delete',
        components: {
            // header: Header,
            default: OrderIndex,
            // footer: Footer
        }
    },

];


var router = new VueRouter({
    routes
});

export default router;
