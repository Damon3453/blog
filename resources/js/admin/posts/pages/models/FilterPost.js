export default {
    pagination: {
        page: 1,
        limit: 20,
    },
    sort: {
        field: 'updated_at',
        order: 0
    },
    fields: {
        id: '',
        heading: '',
        status: '',
        date_range: '',
    }
}
