import VueRouter from 'vue-router';

import UserIndex from './pages/Index.vue'

const routes = [
    {
        path: '/',
        name: 'index',
        components: {
            default: UserIndex,
        }
    }
];


var router = new VueRouter({
    routes
});

export default router;
