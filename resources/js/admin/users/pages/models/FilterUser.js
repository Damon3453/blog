export default {
    pagination: {
        page: 1,
        limit: 20,
    },
    sort: {
        field: 'id',
        order: 1
    },
    fields: {
        id: '',
        name: '',
        surname: '',
        phone: '',
        country: '',
    }
}
