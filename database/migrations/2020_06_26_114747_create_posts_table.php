<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('posts');

        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->index('id');
            $table->string('heading')->nullable();
            $table->text('content')->nullable();
            $table->boolean('active')->default(1);
            $table->timestamp('published_at')->comment('дата публикации');
            $table->string('alias')->nullable();
            $table->string('image')->nullable()->comment('url картинки');
            $table->string('alt')->nullable()->comment('альтернативное название');
            $table->integer('sort')->nullable()->comment('Порядок сортировки');

            $table->string('seo_title')->nullable()->comment('seo заголовок');
            $table->string('seo_description')->nullable()->comment('seo описание');
            $table->string('seo_keywords')->nullable()->comment('seo ключевые слова');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
