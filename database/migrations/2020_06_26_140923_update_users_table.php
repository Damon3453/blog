<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('role')->nullable();
            $table->string('auth_token')->nullable()->default(null)->comment('Токен авторизации');
            $table->timestamp('auth_token_expire')->nullable()->default(null)->comment('Время истечения жизни токена');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role');
            $table->dropColumn('auth_token');
            $table->dropColumn('auth_token_expire');
        });
    }
}
