<?php

use Illuminate\Database\Seeder;

use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'test@test.com',
            'password' => password_hash('12345678', PASSWORD_DEFAULT),
        ]);

        $data = [];
        $data[] = factory(User::class, 100)->create();

        $chunks = array_chunk($data, 10);
        foreach ($chunks as $chunk) {
            User::insert($chunk);
        }
    }
}
