const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/admin/posts/app.js', 'public/js/admin/posts/');
mix.js('resources/js/admin/users/app.js', 'public/js/admin/users/');

mix.webpackConfig({
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'resources/js/'),
            '@posts': path.resolve(__dirname, 'resources/js/admin/posts'),
            '@users': path.resolve(__dirname, 'resources/js/admin/users'),
        }
    }
});

if (mix.inProduction()) {
    mix.version();
}
